<?php

namespace App;

use PHPMailer\PHPMailer\PHPMailer;

class Mail
{
    /**
     * Mail constructor.
     */
    private function __construct()
    {
    }

    /**
     * @param $to
     * @param $subject
     * @param $text
     * @param $html
     */
    public static function send($to, $subject, $text, $html)
    {
        $mail = new PHPMailer();

        $mail->isSMTP();
        $mail->Host = Config::SMTP_HOST;
        $mail->Port = Config::SMTP_PORT;
        $mail->SMTPAuth = true;
        $mail->Username = Config::SMTP_USER;
        $mail->Password = Config::SMTP_PASSWORD;
        $mail->SMTPSecure = Config::SMTP_SECURE;
        $mail->CharSet = 'UTF-8';

        $mail->setFrom(Config::SERVER_EMAIL, Config::SERVER_NAME);
        $mail->addAddress($to);

        $mail->isHTML(true);
        $mail->Subject = $subject;
        $mail->Body = $html;
        $mail->AltBody = $text;

        if ($mail->send()) {

            echo 'Message sent';

        } else {

            echo 'Mailer error: ' . $mail->ErrorInfo;
        }
    }
}