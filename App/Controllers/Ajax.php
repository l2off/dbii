<?php

namespace App\Controllers;

use \App\Models\User;

class Ajax extends \Core\Controller
{
    /**
     * Validate if Username is available (AJAX) for a new signup
     *
     * @return void
     */
    public function validateAccountAction()
    {
        $accountExist = ! User::accountExists($_GET['account']);

        header('Content-Type: application/json');
        echo json_encode($accountExist);
    }

    /**
     * Validate if email is available (AJAX) for a new signup
     *
     * @return void
     */
    public function validateEmailAction()
    {
//        $is_valid = ! User::emailExists($_GET['email']);
//
//        header('Content-Type: application/json');
//        echo json_encode($is_valid);

        $is_valid = ! User::emailExists($_GET['email'], $_GET['ignore_id'] ?? null);

        header('Content-Type: application/json');
        echo json_encode($is_valid);
    }

}