<?php

namespace App\Models;

use \PDO;
use \App\Token;
use \App\Mail;
use \Core\View;
use \Core\Utils;
use \App\Config;
use \App\ValidateRequest;

class User extends \Core\Model
{
    /**
     * Error messages
     *
     * @var array
     */
    public $errors = [];

    /**
     * Class constructor
     *
     * @param array $data  Initial property values (optional)
     */
    public function __construct($data = [])
    {
        foreach ($data as $key => $value) {
            $this->$key = $value;
        };
    }



    /**
     * Save the user model with the current property values
     *
     * @return void
     */
    public function save()
    {
        /**
         *  Server Side Form Validation
         */
        $this->validate();

        if ( isset($_POST['newsletter']) ) {

            $this->newsletter = 1;

        } else {

            $this->newsletter = 0;
        }

        if(empty($this->errors)) {

            /**
             *  Send Email
             */
//            Mail::RegisterEmail($this->email, $this->account, $this->password);




            /* Generate SSN */
            $ssn1 = mt_rand(1000000, 9999999);
            $ssn2 = mt_rand(100000, 999999);
            $ssn = $ssn1 . $ssn2;

            /* dbo.ssn */
            $sql = 'INSERT INTO dbo.ssn (ssn, name, email, newsletter, job, phone, zip, addr_main, addr_etc)
            VALUES (:ssn, :account, :email, :newsletter, :job, :phone, :zip, :addr_main, :addr_etc )';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':ssn', $ssn, PDO::PARAM_STR);
            $stmt->bindValue(':account', $this->account, PDO::PARAM_STR);
            $stmt->bindValue(':email', $this->email, PDO::PARAM_STR);
            $stmt->bindValue(':newsletter', $this->newsletter, PDO::PARAM_STR);
            $stmt->bindValue(':job', $job = 0, PDO::PARAM_STR);
            $stmt->bindValue(':phone', $phone = 0, PDO::PARAM_STR);
            $stmt->bindValue(':zip', $zip = 0, PDO::PARAM_STR);
            $stmt->bindValue(':addr_main', $addr_main = 0, PDO::PARAM_STR);
            $stmt->bindValue(':addr_etc', $addr_etc = 0, PDO::PARAM_STR);

            $stmt->execute();


            /* Encrypt Password */
            $password_encrypt = new Utils();
            $pw = $password_encrypt->encrypt($this->password);

            $InputAnswer1 = new Utils();
            $answer1_encrypt = $InputAnswer1->encrypt($InputAnswer1 = '1');

            $InputAnswer2 = new Utils();
            $answer2_encrypt = $InputAnswer2->encrypt($InputAnswer2 = '2');


            /* dbo.user_auth */
            $sql = 'INSERT INTO dbo.user_auth (account, password, quiz1, quiz2, answer1, answer2)
            VALUES (:account, ' . $pw . ', :quiz1, :quiz2, ' . $answer1_encrypt . ', ' . $answer2_encrypt . ')';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':account', $this->account, PDO::PARAM_STR);
            $stmt->bindValue(':quiz1', $quiz1 = 1, PDO::PARAM_STR);
            $stmt->bindValue(':quiz2', $quiz2 = 2, PDO::PARAM_STR);

            $stmt->execute();


            /* dbo.user_account */

            $token = new Token();
            $hashed_token = $token->getHash();
            $this->activation_token = $token->getValue();

            $sql = 'INSERT INTO dbo.user_account (account, pay_stat, activation_hash )
            VALUES (:account, :pay_stat, :activation_hash )';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':account', $this->account, PDO::PARAM_STR);
            $stmt->bindValue(':pay_stat', $pay_stat = 0, PDO::PARAM_STR);
            $stmt->bindValue(':activation_hash', $hashed_token, PDO::PARAM_STR);

            $stmt->execute();


            /* dbo.user_info */
            $sql = 'INSERT INTO dbo.user_info (account, ssn) VALUES (:account, :ssn)';

            $db = static::getDB();
            $stmt = $db->prepare($sql);

            $stmt->bindValue(':account', $this->account, PDO::PARAM_STR);
            $stmt->bindValue(':ssn', $ssn, PDO::PARAM_STR);

            return $stmt->execute();
        }

        return false;
    }

    /**
     * Validate current property values.
     *
     * @return void
     */
    public function validate()
    {
        /**
         * Username Validation
         */
        if ($this->account == ''){
            $this->errors[] = 'Account is required.';
        }

//        $rules = [
//            'account' => ['required' => true, 'minLength' => 4]
//            ];
//
//        $validate = new ValidateRequest();
//        $validate->abide($_POST, $rules);


        if (strlen($this->account) < Config::MinAccountLength ){
            $this->errors[] = 'Please enter at least ' . Config::MinAccountLength . ' characters for the Username.';
        }

        if (strlen($this->account) > Config::MaxAccountLength ){
            $this->errors[] = 'Please enter at max ' . Config::MaxAccountLength . ' characters for the Username.';
        }

        if (static::accountExists($this->account)){
            $this->errors[] = 'Username already taken.';
        }

        /**
         * Email Validation
         */
        if ($this->email == ''){

            $this->errors[] = 'Email address is required.';

        } else {

            if (filter_var($this->email, FILTER_VALIDATE_EMAIL) == false ){
                $this->errors[] = 'Invalid Email Address.';
            }
        }

        if (static::emailExists($this->email)){
            $this->errors[] = 'Email already taken.';
        }

        /**
         * Password Validation
         */
//         Password Confirmation Match
//        if ($this->password != $this->password_confirmation){
//            $this->errors[] = 'Password must match confirmation.';
//        }

        if (strlen($this->password) < Config::MinPasswordLength ){
            $this->errors[] = 'Please enter at least ' . Config::MinPasswordLength . ' characters for the password.';
        }

        if (strlen($this->password) > Config::MaxPasswordLength ){
            $this->errors[] = 'Please enter at max ' . Config::MaxPasswordLength . ' characters for the password.';
        }

        if (preg_match('/.*[a-z]+.*/i', $this->password) == 0) {
            $this->errors[] = 'Password need at least one letter.';
        }

        if (preg_match('/.*\d+.*/i', $this->password) == 0) {
            $this->errors[] = 'Password need at least one number.';
        }
    }

    /**
     * See if a user record already exists with the specified email
     *
     * @param string $email email address to search for
     *
     * @return boolean  True if a record already exists with the specified email, false otherwise
     */
    public static function emailExists($email)
    {
        return static::findByEmail($email) !== false;
    }

    /**
     * Find a user model by email address
     *
     * @param string $email email address to search for
     *
     * @return mixed User object if found, false otherwise
     */
    public static function findByEmail($email)
    {
        $sql = 'SELECT * FROM dbo.ssn WHERE email = :email';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * Find a user model by Username
     *
     * @param string $account - Username to search for
     *
     * @return mixed User object if found, false otherwise
     */
    public static function findByUsername($account)
    {

        $sql = 'SELECT * FROM dbo.user_auth WHERE account = :account';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':account', $account, PDO::PARAM_STR );

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * Find a user model by ID
     *
     * @param string $id The user ID
     *
     * @return mixed User object if found, false otherwise
     */
    public static function findByID($id)
    {
        $sql = 'SELECT * FROM dbo.user_account WHERE uid = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * @param $account
     * @return bool
     */
    public static function accountExists($account)
    {
        $sql = 'SELECT * FROM dbo.user_auth WHERE account = :account';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindParam(':account', $account, PDO::PARAM_STR);

        $stmt->execute();

        return $stmt->fetch() !== false;
    }

    /**
     * Authenticate a user by email and password. User account has to be active.
     *
     * @param string $account username
     * @param string $password password
     *
     * @return mixed  The user object or false if authentication fails
     */
    public static function authenticate($account, $password)
    {

        $user = static::findByUsername($account);

        if ($user) {

            $encrypt_password = new Utils();
            $pw = $encrypt_password -> encrypt($password);

            // TODO:
            //        if ($user && $user->pay_stat) {

            if( $pw === '0x' . bin2hex($user->password)){
                return $user;
            }
        }

        return false;
    }

    /**
     * Send password reset instructions to the user specified
     *
     * @param string $email The email address
     *
     * @return void
     */
    public static function sendPasswordReset($email)
    {
        $user = static::findByEmail($email);

        if ($user) {

            // Start password reset process here
        }
    }

    /**
     * Send an email to the user containing the activation link
     *
     * @return void
     */
    public function sendActivationEmail()
    {
        $url = 'http://' . $_SERVER['HTTP_HOST'] . '/account/activate/' . $this->activation_token;

        $text = View::getTemplate('emails/activation_email.txt', ['url' => $url]);
        $html = View::getTemplate('emails/activation_email.html', ['url' => $url]);

        Mail::send($this->email, 'Account activation', $text, $html);
    }

    /**
     * Activate the user account with the specified activation token
     *
     * @param string $value Activation token from the URL
     *
     * @return void
     */
    public static function activate($value)
    {
        $token = new Token($value);
        $hashed_token = $token->getHash();

        $sql = 'UPDATE dbo.user_account
                SET pay_stat = 1, activation_hash = null
                WHERE activation_hash = :hashed_token';

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->bindValue(':hashed_token', $hashed_token, PDO::PARAM_STR);

        $stmt->execute();



        //TODO must check that
        /*
        $sql = 'UPDATE dbo.ssn
                SET valid_email_date = getdate()
                WHERE name = :account
                ';

        $stmt->bindValue(':account', $user->account, PDO::PARAM_STR);

        $db = static::getDB();
        $stmt = $db->prepare($sql);

        $stmt->execute();
        */

    }

}