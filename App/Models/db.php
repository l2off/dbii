<?php

namespace App\Models;

use PDO;
use \App\Token;
use \App\Mail;
use \Core\View;
use \Core\Utils;
use \App\Config;
use \App\ValidateRequest;

class dbII extends \Core\Model
{
    /**
     * @param $account
     * @return bool
     */
    public static function accountExists($account)
    {
        return static::findByUsername($account) !== false;
    }

    /**
     * See if a user record already exists with the specified email
     *
     * @param string $email email address to search for
     *
     * @return boolean  True if a record already exists with the specified email, false otherwise
     */
    public static function emailExists($email)
    {
        return static::findByEmail($email) !== false;
    }

    /**
     * @param $id
     * @return mixed
     */
    public static function findByID($id)
    {
        $sql = 'SELECT * FROM dbo.user_account WHERE uid = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * @param $account
     * @return mixed
     */
    public static function findByUsername($account)
    {
        $sql = 'SELECT * FROM dbo.user_auth WHERE account = :account';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':account', $account, PDO::PARAM_STR );

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    /**
     * Find a user model by email address
     *
     * @param string $email email address to search for
     *
     * @return mixed User object if found, false otherwise
     */
    public static function findByEmail($email)
    {
        $sql = 'SELECT * FROM dbo.ssn WHERE email = :email';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }
}