<?php

namespace App\Models;

use \PDO;
use \App\Token;
use \App\Mail;
use \Core\View;
use \Core\Utils;
use \App\Config;

class findBy extends \Core\Model
{
    public static function Username($account)
    {

        $sql = 'SELECT * FROM dbo.user_auth WHERE account = :account';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':account', $account, PDO::PARAM_STR );

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function ID($id)
    {
        $sql = 'SELECT * FROM dbo.user_account WHERE uid = :id';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':id', $id, PDO::PARAM_INT);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function Email($email)
    {
        $sql = 'SELECT * FROM dbo.ssn WHERE email = :email';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':email', $email, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }

    public static function SSN($ssn)
    {
        $sql = 'SELECT * FROM dbo.ssn WHERE ssn = :ssn';

        $db = static::getDB();
        $stmt = $db->prepare($sql);
        $stmt->bindValue(':ssn', $ssn, PDO::PARAM_STR);

        $stmt->setFetchMode(PDO::FETCH_CLASS, get_called_class());

        $stmt->execute();

        return $stmt->fetch();
    }
}