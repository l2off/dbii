# dBII #

PHP MVC Registration & Login System 

### Installing

* PHP 7 (7.2.8)
* PDO SQLSRV

## Deployment

1. Edit ```hosts``` files.
2. Add ```127.0.0.1 dbII```
3. Open ```httpd-vhosts.conf``` file and add the following lines.
```
<VirtualHost *:80>
	DocumentRoot "C:\xampp\htdocs\dbii\public"
    ServerName dBII
</VirtualHost>
```

1. Open ```php.ini``` file.
2. On ```extension``` be sure you have the sql drivers.
```
extension=php_pdo_sqlsrv_72_ts_x86
extension=php_sqlsrv_72_ts_x86
```



1. Go to ```App\``` folder.
2. Rename ```Config.example``` > ```Config.php```.

Config.php example

```
/**
 * Database Driver
 * @var string
 */
const DB_TYPE = 'sqlsrv';

/**
 * Database host
 * @var string
 */
const DB_HOST = '127.0.0.1\L2, 1433';

/**
 * Database name
 * @var string
 */
const DB_NAME = 'lin2db';
    
/**
 * Database user
 * @var string
 */
const DB_USER = 'sa';

/**
 * Database password
 * @var string
 */
const DB_PASSWORD = 'Your SA PASSWORD';
```    

Email Settings
```
/**
 * SMTP host
 *
 * @var string
 */
const SMTP_HOST = 'mail.l2-server-email.com';

/**
 * SMTP port
 *
 * @var int
 */
const SMTP_PORT = 465;

/**
 * SMTP user
 *
 * @var string
 */
const SMTP_USER = 'SMTP UserName';

/**
 * SMTP password
 *
 * @var string
 */
const SMTP_PASSWORD = 'SMTP Password';

/**
 * SMTP Secure
 *
 * @var string
 */
const SMTP_SECURE = 'ssl';
```


## Authors

* MimisK


## License