/**
 *
 */
$(document).ready(function() {

    $('#LoginForm').validate({

        rules: {
            account: {
                required: true
            },
            password: {
                required: true
            }
        }
    });
});