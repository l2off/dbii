
$.validator.addMethod('validPassword',

    function(value, element, param) {
        if(value != ''){

            if(value.match(/.*[a-z]+.*/i) == null ){
                return false;
            }

            if(value.match(/.*\d+.*/) == null ){
                return false;
            }
        }
        return true;
    },
    'Must contain at least one letter and one number'
);

$(document).ready(function(){

    $('#RegisterForm').validate({
        rules: {
            account: {
                required: true,
                remote: '/ajax/validate-account'
            },
            email: {
                required: true,
                email: true,
                remote: '/ajax/validate-email'
            },
            password: {
                required: true,
                minlength: 6,
                validPassword: true
            }
        },
        messages: {
            account: {
                remote: 'Username already taken'
            },
            email: {
                remote: 'Email already taken'
            }
        },
        submitHandler: function(form) {
            $('#signupButton').prop('disabled', true);
            $('#progressImage').show();
            form.submit();
        }
    });

    $('#ForgotForm').validate({
        rules: {
            email: {
                required: true
            }
        }
    });

    $('#ProfileForm').validate({
       rules:{
           name: {
               required: true
           },
           email: {
               required: true
           }
       }
    });

    /**
    * Show password toggle button
    */
    $('#inputPassword').hideShowPassword({
        show: false,
        innerToggle: 'focus',
        toggle: {
            className: 'btn btn-default'
        }
    });

});