<?php

namespace Core;

use \PDO;
use App\Config;

/**
 * Base model
 */
abstract class Model
{
    /**
     * Get the PDO database connection
     * @return mixed
     */
    protected static function getDB()
    {
        /** @var  $db */
        static $db = null;

        if ($db === null)
        {
            $dsn = Config::DB_TYPE . ':Server=' . Config::DB_HOST . ';Database=' . Config::DB_NAME;

            $db = new PDO( $dsn, Config::DB_USER, Config::DB_PASSWORD );

            // Throw an Exception when an error occurs
            $db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return $db;
    }
}