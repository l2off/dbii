'use strict';

var gulp = require('gulp');
var rename = require('gulp-rename');
var del = require('del');
var uglify = require('gulp-uglify');
var sass = require('gulp-sass');
var sourcemaps = require('gulp-sourcemaps');

/**
 * Source Paths
 */
var paths = {
    // Source Destinations
    scripts: ['../src/js/*.js'],
    scss: ['../src/sass/*.scss']
};

/**
* The default task (called when you run `gulp` from cli)
* gulp.task('default', ['watch', 'scripts']);
*/
gulp.task('default', ['build:scripts', 'build:sass']);



/**
 * Build: JavaScript files.
 */
gulp.task('build:scripts', function() {

    return gulp.src(paths.scripts)
    // .pipe(sourcemaps.init())
        .pipe(uglify())
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('../public/assets/js'));
});

/**
 * Build: Stylesheet files.
 */
gulp.task('build:sass', function () {

    return gulp.src(paths.scss)
        .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
        .pipe(rename({
            suffix: '.min'
        }))
        .pipe(gulp.dest('../public/assets/css'));
});



/**
 * Clean: Delete JavaScript & StyleSheet files.
 */
gulp.task('clean', function() {

    return del(
        ['../public/assets/js/*.js', '../public/assets/css/*.css'],
        {force: true}
    );
});

/**
 * Clean: Delete Build Javascript files.
 */
gulp.task('clean:scripts', function() {

    return del(['../public/assets/js/*.js'], {force: true});
});

/**
 * Clean: Delete Build Stylesheet files.
 */
gulp.task('clean:sass', function() {

    return del(['../public/assets/css/*.css'], {force: true});
});



/**
 * Watch: JavaScript & Sass
 * Rerun the task when a file changes.
 */
gulp.task('watch', function() {

    gulp.watch(paths.scripts, ['scripts', 'sass']);
});

/**
 * Watch: JavaScript
 * Rerun the task when a file changes.
 */
gulp.task('watch:scripts', function() {

    gulp.watch(paths.scripts, ['scripts']);
});

/**
 * Watch: SCSS
 * Rerun the task when a file changes.
 */
gulp.task('watch:sass', function() {

    gulp.watch(paths.scss, ['sass']);
});
